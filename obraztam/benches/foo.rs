use criterion::{black_box, criterion_group, criterion_main, BenchmarkId, Criterion};
use obraztam::data::Stack;
use obraztam::{Array2d, Collectible};

pub fn criterion_benchmark(c: &mut Criterion) {
    let a = Stack::<_, 4>::new_from_arr(
        [black_box(1); 4],
        (2, 2),
    ).unwrap();
    // this is super slowest
    c.bench_with_input(
        BenchmarkId::new("ref3", ""),
        &a,
        |b, a: &Stack::<_, 4>| b.iter(|| {
            a
                .map(|i| i + 1)
                .map(|i| i + 44)
                .collect::<Stack::<_, 4>>()
        })
    );
    // this is 2x as slow as no-reference one. odd.
    c.bench_function("t4", |b| b.iter(|| {
        let a = Stack::<_, 4>::new_from_arr(
            [black_box(1); 4],
            (2, 2),
        ).unwrap();
        (&a)
            .map(|i| i + 1)
            .map(|i| i + 44)
            .collect::<Stack::<_, 4>>()
    }));
    /* uncomment to see t4 go down to same as t1, but ref3 go up
     * 
     * c.bench_function("t5", |b| b.iter(|| {
        let a = Stack::<_, 4>::new_from_arr(
            [black_box(1); 4],
            (2, 2),
        ).unwrap();
        a.clone()
            .map(|i| i + 1)
            .map(|i| i + 44)
            .collect::<Stack::<_, 4>>()
    }));*/
    c.bench_function("t1", |b| b.iter(|| {
        let a = Stack::<_, 4>::new_from_arr(
            [black_box(1); 4],
            (2, 2),
        ).unwrap();
        a
            .map(|i| i + 1)
            .map(|i| i + 44)
            .collect::<Stack::<_, 4>>()
    }));
    c.bench_function("t2", |b| b.iter(|| {
        let a = Stack::<_, 4>::new_from_arr(
            [black_box(1); 4],
            (2, 2),
        ).unwrap();
        a
            .map(|i| i + 44)
            .collect::<Stack::<_, 4>>()
    }));
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);