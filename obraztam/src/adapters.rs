use crate::{Array2d, Collectible};
use crate::elementwise;
use crate::index::Absolute;

use std::marker::PhantomData;
    

/// Deferred processing
pub struct Map<A, F, O> {
    array: A,
    f: F,
    out_type: PhantomData<O>,
}

impl<A, F, O> Map<A, F, O> {
    pub fn new(array: A, f: F) -> Self {
        Map {
            array,
            f,
            out_type: Default::default(),
        }
    }
}

impl<T, A: Array2d, F: Fn(A::Item) -> T> Array2d for Map<A, F, T> {
    type Item = T;
    fn shape(&self) -> (u32, u32) {
        self.array.shape()
    }
    #[inline]
    fn index(&self, position: (u32, u32)) -> T {
        (self.f)(self.array.index(position))
    }
}

impl<T, A: Array2d, F: Fn(A::Item) -> T> Collectible for Map<A, F, T> {}


pub struct MapIndex<A, F, O> {
    array: A,
    f: F,
    out_type: PhantomData<O>,
}

impl<A, F, O> MapIndex<A, F, O> {
    pub fn new(array: A, f: F) -> Self {
        MapIndex {
            array,
            f,
            out_type: Default::default(),
        }
    }
}

impl<T, A: Array2d, F: Fn((u32, u32), A::Item) -> T> Array2d for MapIndex<A, F, T> {
    type Item = T;
    fn shape(&self) -> (u32, u32) {
        self.array.shape()
    }
    #[inline]
    fn index(&self, position: (u32, u32)) -> T {
        (self.f)(position, self.array.index(position))
    }
}

impl<T, A: Array2d, F: Fn((u32, u32), A::Item) -> T> Collectible for MapIndex<A, F, T> {}



/// This is the universal adapter. Access to all image, write to indexed cell.
pub struct M<A, F, O> {
    array: A,
    f: F,
    offset: (i32, i32),
    out_type: PhantomData<O>,
}

impl<A, F: elementwise::Resizes<A, O>, O> M<A, F, O> {
    pub fn new(array: A, f: F) -> Self {
        let offset = f.get_extent(&array).start;
        M {
            array,
            f,
            offset,
            out_type: Default::default(),
        }
    }
}

impl<O, A: Array2d, F: elementwise::Resizes<A, O>> Array2d for M<A, F, O> {
    type Item = O;
    fn shape(&self) -> (u32, u32) {
        self.f.get_shape(&self.array)
    }
    #[inline]
    fn index(&self, position: (u32, u32)) -> O {
        self.f.call(position.add(self.offset), &self.array)
    }
}

impl<O, A: Array2d, F: elementwise::Resizes<A, O>> Collectible for M<A, F, O> {}


