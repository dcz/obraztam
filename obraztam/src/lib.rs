/*! Applicative functors in the service of 2d array procesing.
 * Attempt 2, now Rust has generic associated types. */

mod adapters;
pub mod data;
pub mod elementwise;
pub mod index;
pub mod ops;

use adapters::{Map, MapIndex, M};
pub use index::{Index, Offset};

/// An applicative functor trait.
/// Processes array elements in a chained way.
/// That includes convolution.
/// Offers some convenience functions.
pub trait Array2d where Self: Sized {
    type Item;
    fn shape(&self) -> (u32, u32);
    fn index(&self, position: (u32, u32)) -> Self::Item;
    
    fn map<U, F: Fn(Self::Item) -> U>(self, f: F) -> Map<Self, F, U> {
        Map::new(self, f)
    }
    fn map_index<U, F: Fn((u32, u32), Self::Item) -> U>(self, f: F) -> MapIndex<Self, F, U> {
        MapIndex::new(self, f)
    }
    fn m<O, F: elementwise::Resizes<Self, O>>(self, f: F) -> M<Self, F, O> {
        M::new(self, f)
    }
}

impl<T> Array2d for &T where T: Array2d {
    type Item = T::Item;

    fn shape(&self) -> Index {
        (*self).shape()
    }
    #[inline]
    fn index(&self, position: (u32, u32)) -> Self::Item {
        (*self).index(position)
    }
}

pub trait MutArray<Item> {
    fn new(shape: (u32, u32)) -> Self;
    fn set(&mut self, position: (u32, u32), value: Item);
}

/// Collect() makes this all tick fast.
/// It allows the compiler to chain operations and optimize them before evaluating.
pub trait Collectible where Self: Array2d {
    fn collect<A: MutArray<Self::Item>>(self) -> A {
        let (x, y) = self.shape();
        let mut out = A::new((x, y));
        for i in 0..y {
            for j in 0..x {
                out.set((j, i), self.index((j, i)));
            }
        }
        out
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::data::Stack;

    type MyImg<T> = Stack<T, 4>;
    
    #[test]
    fn reference() {
        let a = MyImg::<_>::new_from_arr([3; 4], (2, 2)).unwrap();
        assert_eq!(
            (&a)
                .map(|i| i + 1)
                .map(|i| i + 44)
                .collect::<MyImg<_>>(),
            MyImg::<_>::new_from_arr([48; 4], (2, 2)).unwrap()
        );
        assert_eq!(
            (&a)
                .map(|i| i + 1)
                .map(|i| i + 44)
                .collect::<MyImg<_>>(),
            MyImg::<_>::new_from_arr([48; 4], (2, 2)).unwrap()
        );
    }
    
    #[test]
    fn map() {
        let a = MyImg::<_>::new_from_arr([3; 4], (2, 2)).unwrap();
        assert_eq!(
            a
                .map(|i| i + 1)
                .map(|i| i + 44)
                .collect::<MyImg<_>>(),
            MyImg::<_>::new_from_arr([48; 4], (2, 2)).unwrap()
        );
    }
    
    #[test]
    fn map_index() {
        let a = MyImg::<_>::new_from_arr([3; 4], (2, 2)).unwrap();
        assert_eq!(
            a
                .map_index(|_, i| i + 1)
                .map_index(|_, i| i + 44)
                .collect::<MyImg<_>>(),
            MyImg::<_>::new_from_arr([48; 4], (2, 2)).unwrap()
        );
    }
    
    #[test]
    fn map_as_map_index() {
        use elementwise::map;
        let a = MyImg::<_>::new_from_arr([3; 4], (2, 2)).unwrap();
        assert_eq!(
            a
                .m(map(|i| i + 1))
                .m(map(|i| i + 44))
                .collect::<MyImg<_>>(),
            MyImg::<_>::new_from_arr([48; 4], (2, 2)).unwrap()
        );
    }

    
    #[test]
    fn zip() {
        use elementwise::map::zip;
        let a = MyImg::<_>::new_from_arr([3; 4], (2, 2)).unwrap();
        let b = MyImg::<_>::new_from_arr([2; 4], (2, 2)).unwrap();
        assert_eq!(
            a
                .map_index(zip(b))
                .collect::<MyImg<_>>(),
            MyImg::<_>::new_from_arr([(3, 2); 4], (2, 2)).unwrap()
        );
    }
    
    #[test]
    fn zip_all() {
        use elementwise::zip;
        let a = MyImg::<_>::new_from_arr([3; 4], (2, 2)).unwrap();
        let b = MyImg::<_>::new_from_arr([2; 4], (2, 2)).unwrap();
        assert_eq!(
            a
                .m(zip(b))
                .collect::<MyImg<_>>(),
            MyImg::<_>::new_from_arr([(3, 2); 4], (2, 2)).unwrap()
        );
    }
}
