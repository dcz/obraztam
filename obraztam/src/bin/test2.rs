use obraztam::data::Stack;
use obraztam::{Array2d, Collectible};
use std::env;

fn main() {
    type MyImg<T> = Stack<T, 4>;
    let a = MyImg::new_from_arr(
        [env::args().len() as u8; 4],
        (2, 2),
    ).unwrap();
    // seems to be using SIMD when some critical funcs are inlined.
    // also seems to be the same as .map().
    assert_eq!(
        a
            .map_index(|_, i| i + 1)
            .map_index(|_, i| i + 44)
            .collect::<MyImg::<_>>(),
        MyImg::new_from_arr([48; 4], (2, 2)).unwrap()
    );
}