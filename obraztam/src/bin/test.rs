use obraztam::data::Stack;
use obraztam::{Array2d, Collectible};
use std::env;

fn main() {
    let a = Stack::<_, 4>::new_from_arr(
        [env::args().len() as u8; 4],
        (2, 2),
    ).unwrap();
    // seems to be using SIMD when some critical funcs are inlined
    assert_eq!(
        a
            .map(|i| i + 1)
            .map(|i| i + 44)
            .collect::<Stack::<_, 4>>(),
        Stack::<_, 4>::new_from_arr([48; 4], (2, 2)).unwrap(),
    );
}