/* Traits for image manipulation */

use crate::Array2d;
use crate::adapters::Map;


pub trait RGBPixel<I> {
    fn r(&self) -> I;
    fn g(&self) -> I;
    fn b(&self) -> I;
}
pub trait HSLPixel<I> {
    fn h(&self) -> I;
    fn s(&self) -> I;
    fn l(&self) -> I;
}

pub trait RGBImage<Item, C>
    where
    Self: Array2d<Item=Item>,
    Item: RGBPixel<C>,
{
    fn to_hsl<O: HSLPixel<C>>(self) -> Map<Self, fn(Item) -> O, O> {
        
    }
}
