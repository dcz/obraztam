/*! Image-compatible data structures.
Stack are well suited for small things. The don't allocate but may waste memory. */

use crate::{Array2d, MutArray, Collectible, Index};

/// By default can store up to 256 elements (e.g. 16x16)
#[derive(Debug, Clone)]
pub struct Stack<T, const S: usize = 256usize> {
    data: [T; S],
    shape: Index,
}

impl<T: PartialEq, const S: usize> PartialEq for Stack<T, S> {
    fn eq(&self, other: &Self) -> bool {
        let (a, b) = self.shape;
        let count = (a * b) as usize;
        self.shape == other.shape
            && self.data[..count] == other.data[..count]
    }
}

impl<T: Copy, const S: usize> Stack<T, S> {
    pub fn new_from_arr(data: [T; S], shape: Index) -> Result<Self, [T; S]> {
        let (a, b) = shape;
        if (a * b) as usize <= data.len() {
            Ok(Self { data, shape })
        } else {
            Err(data)
        }
    }

    #[inline]
    fn to_offset(&self, (xp, yp): (u32, u32)) -> usize {
        let (_x, y) = self.shape();
        (xp*y + yp) as usize
    }
}

impl<T: Copy, const S: usize> Array2d for Stack<T, S> {
    type Item = T;
    #[inline]
    fn shape(&self) -> (u32, u32) { self.shape }
    #[inline]
    fn index(&self, pos: (u32, u32)) -> Self::Item {
        self.data[self.to_offset(pos)]
    }
}

impl<T: Default + Copy, const S: usize> MutArray<T> for Stack<T, S> {
    fn new(shape: Index) -> Self {
        Self::new_from_arr([T::default(); S], shape).ok().unwrap()
    }
    #[inline]
    fn set(&mut self, pos: (u32, u32), value: T) {
        self.data[self.to_offset(pos)] = value;
    }
}

impl<T: Copy, const S: usize> Collectible for Stack<T, S> {}


#[derive(Debug, PartialEq)]
pub struct Heap<T> {
    data: Vec<T>,
    shape: Index,
}

impl<T: Copy> Heap<T> {
    pub fn new_from_vec(data: Vec<T>, shape: Index) -> Result<Self, Vec<T>> {
        let (a, b) = shape;
        if (a * b) as usize == data.len() {
            Ok(Self { data, shape })
        } else {
            Err(data)
        }
    }

    #[inline]
    fn to_offset(&self, (xp, yp): (u32, u32)) -> usize {
        let (_x, y) = self.shape();
        (xp*y + yp) as usize
    }
}

impl<T: Copy> Array2d for Heap<T> {
    type Item = T;
    #[inline]
    fn shape(&self) -> (u32, u32) { self.shape }
    #[inline]
    fn index(&self, pos: (u32, u32)) -> Self::Item {
        self.data[self.to_offset(pos)]
    }
}

impl<T: Default + Copy> MutArray<T> for Heap<T> {
    fn new(shape: Index) -> Self {
        let (x, y) = shape;
        let len = (x * y) as usize;
        let mut v = Vec::with_capacity(len);
        for _i in 0..len {
            v.push(T::default());
        }
        Self::new_from_vec(v, shape).ok().unwrap()
    }
    #[inline]
    fn set(&mut self, pos: (u32, u32), value: T) {
        let i = self.to_offset(pos);
        self.data[i] = value;
    }
}

impl<T: Copy> Collectible for Heap<T> {}


#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn collect() {
        let a = Stack::new_from_arr([3; 4], (2, 2)).unwrap();
        assert_eq!(
            a.collect::<Stack::<_, 4>>(),
            Stack::new_from_arr([3; 4], (2, 2)).unwrap()
        );
    }
    #[test]
    fn dead() {
        assert_eq!(
            Stack::new_from_arr([1, 2, 3, 4], (2, 1)).unwrap(),
            Stack::new_from_arr([1, 2, 3, 5], (2, 1)).unwrap(),
        );
    }
    #[test]
    fn hcollect() {
        let a = Heap::new_from_vec(vec![3, 3, 3, 3], (2, 2)).unwrap();
        assert_eq!(
            a.collect::<Heap::<_>>(),
            Heap::new_from_vec(vec![3, 3, 3, 3], (2, 2)).unwrap()
        );
    }
}