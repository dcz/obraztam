/*! Ops useable in the context of Array2d::m
 *
 * This is quite awful, contains operation functions.
Use them like: Array2d::map(div(9))
Ideally, this would work sth like Array2d::div(9),
but there are several layers of problems with that.
First, ::div cannot expand to self.map(|foo| foo / 9),
because we don't evaluate eagerly.
That means we return the closure,
and need to have the closure type in the function signature.
This is not allowed, because the type is created inside the function, while the caller specifies the type.
https://github.com/rust-lang/rust/issues/51154
We also cannot do impl Fn(...) because that's not allowed in traits.
Finally, Box<dyn Fn(...)> blocks compiler optimizations, and we want this interface to be like Iterator.
We dont want to evaluate eagerly, because we change types, and we want to avoid allocations.
*/

pub mod map;
use core::cmp::{min, max};
use crate::{Array2d, Index};

pub fn map<I, O, A: Array2d<Item=I>>(f: impl Fn(I) -> O) -> impl Fn(Index, &A) -> O {
    move |index, a| f(a.index(index))
}

pub fn zip<IA, IB, A: Array2d<Item=IA>, B: Array2d<Item=IB>>(b: B)
    -> impl Fn(Index, &A) -> (IA, IB)
{
    move |i, a| (a.index(i), b.index(i))
}

/// A rectangle in the image
#[derive(Debug, PartialEq)]
pub struct Extent {
    pub start: (i32, i32),
    pub end: (i32, i32),
}

impl Extent {
    #[inline]
    pub fn get_size(&self) -> Index {
        let (a_s, b_s) = self.start;
        let (a_e, b_e) = self.end;
        (
            a_e.saturating_sub(a_s) as u32,
            b_e.saturating_sub(b_s) as u32,
        )
    }
}

/// Outputs an image with a different extent than original.
pub trait Resizes<A, R> {
    // TODO: this should be fallible in case the operation does not make sense on this array (e.g. need more size)
    fn get_extent(&self, a: &A) -> Extent;
    #[inline]
    fn get_shape(&self, a: &A) -> Index {
        self.get_extent(a).get_size()
    }
    fn call(&self, index: Index, a: &A) -> R;
}

/// Blanket impl for the basic function, traverses the original size
impl<I, A, R, T> Resizes<A, R> for T 
where 
    A: Array2d<Item=I>,
    T: Fn(Index, &A) -> R,
{
    #[inline]
    fn get_extent(&self, a: &A) -> Extent {
        Extent {start: (0, 0), end: {
            let s = a.shape();
            (s.0 as i32, s.1 as i32)
        }}
    }
    #[inline]
    fn call(&self, index: Index, a: &A) -> R {
        self(index, a)
    }
}


/// Scales by the factor in a nearest-neighbor fashion.
/// Don't give it odd dimensions %)
struct Shrink(u32);

impl<A: Array2d<Item = R>, R> Resizes<A, R> for Shrink {
    fn get_extent(&self, a: &A) -> Extent {
        let (d1, d2) = a.shape();
        Extent {
            start: (0, 0),
            end: ((d1 / self.0) as i32, (d2 / self.0) as i32),
        }
    }
    #[inline]
    fn call(&self, (x, y): Index, a: &A) -> R {
        a.index((x * self.0, y * self.0))
    }
}

struct Overlap<T: Array2d>{
    template: T,
    position: (i32, i32),
}

impl<IT, IA, A, T> Resizes<A, (IA, IT)> for Overlap<T>
where 
    A: Array2d<Item=IA>,
    T: Array2d<Item=IT>,
{
    fn get_extent(&self, a: &A) -> Extent {
        let (a1, a2) = a.shape();
        let (t1, t2) = self.template.shape();
        let (p1, p2) = self.position;
        Extent {
            start: (max(0, p1), max(0, p2)),
            end: (
                min(a1 as i32, t1 as i32 + p1),
                min(a2 as i32, t2 as i32 + p2),
            ),
        }
    }
    #[inline]
    fn call(&self, (i1, i2): Index, a: &A) -> (IA, IT) {
        let (p1, p2) = self.position;
        (
            a.index(dbg!(i1, i2)),
            self.template.index((
                (i1 as i32 - p1) as u32,
                (i2 as i32 - p2) as u32,
            ))
        )
    }
}    


struct Convolve<P: Array2d>(P);

impl<O, A, Pat> Resizes<A, O> for Convolve<Pat>
where 
    A: Array2d<Item=O>,
    Pat: Array2d<Item=O>,
{
    fn get_extent(&self, a: &A) -> Extent {
        let (a1, a2) = a.shape();
        let (p1, p2) = self.0.shape();
        Extent {
            start: (0, 0),
            end: ((a1 - p1) as i32, (a2 - p2) as i32),
        }
    }
    #[inline]
    fn call(&self, (x, y): Index, a: &A) -> O {
        panic!()
    }
}    

#[cfg(test)]
mod tests {
    use super::*;
    use crate::data::Stack;
    use crate::Collectible;

    #[test]
    fn resize1() {
        let a = Stack::new_from_arr([3; 4], (4, 1)).unwrap();
        assert_eq!(
            a.m(Shrink(1))
                .collect::<Stack::<_, 4>>(),
            Stack::new_from_arr([3; 4], (4, 1)).unwrap()
        );
    }

    #[test]
    fn resize2() {
        let a = Stack::new_from_arr([3; 8], (4, 2)).unwrap();
        assert_eq!(
            a.m(Shrink(2))
                .collect::<Stack::<_, 8>>(),
            Stack::new_from_arr([3; 8], (2, 1)).unwrap()
        );
    }

    #[test]
    fn shrink2() {
        let a = Stack::new_from_arr([1, 2, 3, 4, 5, 6, 7, 8], (4, 2)).unwrap();
        dbg!(a.index((1, 0)));
        dbg!(a.index((2, 0)));
        assert_eq!(
            a.m(Shrink(2))
                .collect::<Stack::<_, 2>>(),
            Stack::new_from_arr([1, 5], (2, 1)).unwrap()
        );
    }
    
    #[test]
    fn overlap_extent() {
        let a = Stack::new_from_arr([1; 4], (2, 2)).unwrap();
        let t = Stack::new_from_arr([1; 4], (2, 2)).unwrap();
        let o = Overlap { template: t.clone(), position: (0, 0) };
        assert_eq!(
            o.get_extent(&a),
            Extent { start: (0, 0), end: (2, 2) },
        );
        assert_eq!(
            a.m(o).collect::<Stack::<_, 4>>(),
            Stack::new_from_arr([(1, 1); 4], (2, 2)).unwrap(),
        );
    }
    #[test]
    fn overlap_extent_p() {
        let a = Stack::new_from_arr([1; 4], (2, 2)).unwrap();
        let t = Stack::new_from_arr([1; 4], (2, 2)).unwrap();

        let o = Overlap { template: t.clone(), position: (1, 0) };
        assert_eq!(
            o.get_extent(&a),
            Extent { start: (1, 0), end: (2, 2) },
        );
        assert_eq!(
            a.m(o).collect::<Stack::<_, 4>>(),
            Stack::new_from_arr([(1, 1); 4], (1, 2)).unwrap(),
        );
    }
    #[test]
    fn overlap_extent_n() {
        let a = Stack::new_from_arr([1; 4], (2, 2)).unwrap();
        let t = Stack::new_from_arr([1; 4], (2, 2)).unwrap();

        let o = Overlap { template: t.clone(), position: (-1, 0) };
        assert_eq!(
            o.get_extent(&a),
            Extent { start: (0, 0), end: (1, 2) },
        );
    }
    #[test]
    fn overlap_extent_smaller() {
        let a = Stack::new_from_arr([1; 4], (4, 1)).unwrap();
        let t = Stack::new_from_arr([1; 4], (2, 1)).unwrap();

        let o = Overlap { template: t.clone(), position: (0, 0) };
        assert_eq!(
            o.get_extent(&a),
            Extent { start: (0, 0), end: (2, 1) },
        );
    }
    #[test]
    fn overlap_extent_smaller_m() {
        let a = Stack::new_from_arr([1; 4], (4, 1)).unwrap();
        let t = Stack::new_from_arr([1; 4], (2, 1)).unwrap();

        let o = Overlap { template: t.clone(), position: (-1, 0) };
        assert_eq!(
            o.get_extent(&a),
            Extent { start: (0, 0), end: (1, 1) },
        );
    }
    #[test]
    fn overlap_extent_smaller_p() {
        let a = Stack::new_from_arr([1; 4], (4, 1)).unwrap();
        let t = Stack::new_from_arr([1; 4], (2, 1)).unwrap();

        let o = Overlap { template: t.clone(), position: (1, 0) };
        assert_eq!(
            o.get_extent(&a),
            Extent { start: (1, 0), end: (3, 1) },
        );
    }
    #[test]
    fn overlap_extent_smaller_p_ovf() {
        let a = Stack::new_from_arr([1; 4], (4, 1)).unwrap();
        let t = Stack::new_from_arr([1; 4], (2, 1)).unwrap();

        let o = Overlap { template: t.clone(), position: (3, 0) };
        assert_eq!(
            o.get_extent(&a),
            Extent { start: (3, 0), end: (4, 1) },
        );
    }
    #[test]
    fn overlap_extent_bigger() {
        let a = Stack::new_from_arr([1; 4], (2, 1)).unwrap();
        let t = Stack::new_from_arr([1; 4], (4, 1)).unwrap();

        let o = Overlap { template: t.clone(), position: (0, 0) };
        assert_eq!(
            o.get_extent(&a),
            Extent { start: (0, 0), end: (2, 1) },
        );
    }
    #[test]
    fn overlap_extent_bigger_m() {
        let a = Stack::new_from_arr([1; 4], (2, 1)).unwrap();
        let t = Stack::new_from_arr([1; 4], (4, 1)).unwrap();

        let o = Overlap { template: t.clone(), position: (-1, 0) };
        assert_eq!(
            o.get_extent(&a),
            Extent { start: (0, 0), end: (2, 1) },
        );
    }
    #[test]
    fn overlap_extent_bigger_m_ovf() {
        let a = Stack::new_from_arr([1, 2, 0, 0], (2, 1)).unwrap();
        let t = Stack::new_from_arr([1, 2, 3, 4], (4, 1)).unwrap();

        let o = Overlap { template: t.clone(), position: (-3, 0) };
        assert_eq!(
            o.get_extent(&a),
            Extent { start: (0, 0), end: (1, 1) },
        );
        assert_eq!(
            a.m(o).collect::<Stack::<_, 4>>(),
            Stack::new_from_arr([(1, 4); 4], (1, 1)).unwrap(),
        );
    }
    #[test]
    fn overlap_extent_bigger_p() {
        let a = Stack::new_from_arr([1; 4], (2, 1)).unwrap();
        let t = Stack::new_from_arr([1; 4], (4, 1)).unwrap();

        let o = Overlap { template: t.clone(), position: (1, 0) };
        assert_eq!(
            o.get_extent(&a),
            Extent { start: (1, 0), end: (2, 1) },
        );
    }
}