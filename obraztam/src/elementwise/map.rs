/*! Ops useable in the context of Array2d::map */

use crate::{Array2d, Index};
use std::ops::Div;

pub fn div<O: Copy, T: Div<O>>(value: O) -> impl Fn(T) -> T::Output {
    move |t| t / value
}



/// To plug into map_index
pub fn map<I, O>(f: impl Fn(I) -> O) -> impl Fn((u32, u32), I) -> O {
    move |_, v| f(v)
}

pub fn zip<IA, IB, B: Array2d<Item=IB>>(b: B)
    -> impl Fn(Index, IA) -> (IA, IB)
{
    move |i, v| (v, b.index(i))
}
