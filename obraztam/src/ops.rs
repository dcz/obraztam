/*! Giving up. Instead of chaining bar().foo(), this offers foo(bar())).
 * This is because bar() turns out to require some extra syntax to work.
 */

use crate::Array2d;
use crate::adapters::MapIndex;

pub fn map<I, O, A: Array2d<Item=I>, F: Fn(I) -> O>(
    array: A,
    f: F,
) -> MapIndex<A, impl Fn((u32, u32), I) -> O, O> {
    MapIndex::new(array, move |_, i| f(i))
}

#[cfg(test)]
mod test {
    use super::*;
    
    use crate::Collectible;
    use crate::data::Stack;

    type MyImg<T> = Stack<T, 4>;

  
    #[test]
    fn map() {
        let a = MyImg::<_>::new_from_arr([3; 4], (2, 2)).unwrap();
        assert_eq!(
            a
                .map(|i| i + 1)
                .map(|i| i + 44)
                .collect::<MyImg::<_>>(),
            MyImg::<_>::new_from_arr([48; 4], (2, 2)).unwrap()
        );
    }
}