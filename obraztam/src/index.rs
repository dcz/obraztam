/*! Index manipulation tools */

pub type Index = (u32, u32);
pub type Offset = (i32, i32);

pub trait Absolute {
    type Relative;
    fn add(&self, other: Self::Relative) -> Self;
    fn sub(&self, other: Self::Relative) -> Self;
}

impl Absolute for Index {
    type Relative = Offset;
    #[inline]
    fn add(&self, other: Self::Relative) -> Self {
        let (a1, a2) = *self;
        let (o1, o2) = other;
        (
            (a1 as i32).saturating_add(o1).try_into().unwrap(),
            (a2 as i32).saturating_add(o2).try_into().unwrap(),
        )
    }
    #[inline]
    fn sub(&self, other: Self::Relative) -> Self {
        let (a1, a2) = *self;
        let (o1, o2) = other;
        (
            (a1 as i32).saturating_sub(o1).try_into().unwrap(),
            (a2 as i32).saturating_sub(o2).try_into().unwrap(),
        )
    }
}
