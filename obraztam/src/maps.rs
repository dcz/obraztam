use crate::applimage::{ Binary2d, IntoFunctor };
use image;
use image::Pixel;
use imageproc;
use itertools::Itertools;
use std::cmp;
use std::cmp::max;
use std::fmt;
use std::ops::Add;


use std::fmt::Write;


pub type BinaryMap = image::GrayImage;

#[derive(Clone)]
pub struct IntensityMap(pub image::GrayImage);

// Can't be a trait impl because GrayImage is not defined here.
pub fn sub(from: &BinaryMap, without: &BinaryMap) -> BinaryMap {
    imageproc::map::map_colors2(
        from,
        without,
        |f, w| image::Luma::from([cmp::min(f[0], 1).saturating_sub(w[0])])
    )
}

impl IntensityMap {
    pub fn new_from_value(size: &(u16, u16), value: u8) -> IntensityMap {
        IntensityMap(image::GrayImage::from_pixel(size.0 as u32, size.1 as u32, image::Luma::from([value])))
    }
    
    pub fn new_from_mask(mask: &BinaryMap, value: u8) -> IntensityMap {
        IntensityMap(imageproc::map::map_colors(
            mask,
            |p| image::Luma::from([if p[0] > 0 { value } else { 0 }])
        ))
    }
}


impl<'a> Into<&'a BinaryMap> for &'a IntensityMap {
    fn into(self) -> &'a BinaryMap {
        &self.0
    }
}

impl Add<IntensityMap> for IntensityMap {
    type Output = IntensityMap;
    fn add(self, other: IntensityMap) -> Self::Output {
        IntensityMap(imageproc::map::map_colors2(
            &self.0,
            &other.0,
            |s, o| image::Luma::from([s[0] + o[0]])
        ))
    }
}

impl IntoFunctor<Binary2d> for IntensityMap {
    fn process(self) -> Binary2d {
        Binary2d {
            data: self.0,
        }
    }
}

pub fn mask(i: IntensityMap, mask: &BinaryMap) -> IntensityMap {
    IntensityMap(imageproc::map::map_colors2(
        &i.0,
        mask,
        |i, m| image::Luma::from([
            if m[0] > 0 { i[0] } else { 0 }
        ]),
    ))
}

pub fn print_image<I, P>(image: &I)
    where
        P: Pixel,
        P::Subpixel: fmt::Debug,
        I: image::GenericImage<Pixel = P> {
    let (x, y) = image.dimensions();
    println!(
        "{}",
        render_image_region(
            image,
            0, 0,
            x - 1, y - 1,
        )
    );
}

pub fn render_image_region<I, P>(
    image: &I,
    left: u32,
    top: u32,
    right: u32,
    bottom: u32,
) -> String
where
    P: Pixel,
    P::Subpixel: fmt::Debug,
    I: image::GenericImage<Pixel = P>,
{
    let mut rendered = String::new();

    // Render all the pixels first, so that we can determine the column width
    let mut rendered_pixels = vec![];
    for y in top..bottom + 1 {
        for x in left..right + 1 {
            let p = image.get_pixel(x, y);
            rendered_pixels.push(render_pixel(p));
        }
    }

    // Width of a column containing rendered pixels
    let pixel_column_width = rendered_pixels.iter().map(|p| p.len()).max().unwrap() + 1;
    // Maximum number of digits required to display a row or column number
    let max_digits = (max(1, max(right, bottom)) as f64).log10().ceil() as usize;
    // Each pixel column is labelled with its column number
    let pixel_column_width = pixel_column_width.max(max_digits + 1);
    let num_columns = (right - left + 1) as usize;

    // First row contains the column numbers
    write!(rendered, "\n{}", " ".repeat(max_digits + 4)).unwrap();
    for x in left..right + 1 {
        write!(rendered, "{x:>w$} ", x = x, w = pixel_column_width).unwrap();
    }

    // +--------------
    write!(
        rendered,
        "\n  {}+{}",
        " ".repeat(max_digits),
        "-".repeat((pixel_column_width + 1) * num_columns + 1)
    )
    .unwrap();
    // row_number |
    write!(rendered, "\n  {y:>w$}| ", y = " ", w = max_digits).unwrap();

    let mut count = 0;
    for y in top..bottom + 1 {
        // Empty row, except for leading | separating row numbers from pixels
        write!(rendered, "\n  {y:>w$}| ", y = y, w = max_digits).unwrap();

        for x in left..right + 1 {
            // Pad pixel string to column width and right align
            let padded = format!(
                "{c:>w$}",
                c = rendered_pixels[count],
                w = pixel_column_width
            );
            write!(rendered, "{} ", &padded).unwrap();
            count += 1;
        }
        // Empty row, except for leading | separating row numbers from pixels
        write!(rendered, "\n  {y:>w$}| ", y = " ", w = max_digits).unwrap();
    }
    rendered.push('\n');
    rendered
}

fn render_pixel<P>(p: P) -> String
where
    P: Pixel,
    P::Subpixel: fmt::Debug,
{
    let cs = p.channels();
    match cs.len() {
        1 => format!("{:?}", cs[0]),
        _ => format!("[{}]", cs.iter().map(|c| format!("{:?}", c)).join(", ")),
    }
}
