/*! Applicative functors in the service of 2d array procesing */

use crate::maps;
pub mod neighborhoods;

pub trait IntoFunctor<F: Array2d> {
    fn process(self) -> F;
}

/// An applicative functor trait.
/// Processes array elements in a chained way.
/// That includes convolution.
/// Offers some convenience functions.
///
/// The implementor can choose how to process the data:
/// immediately, or deferred until collect() is called, or in parallel.
// This could *really* use higher-kinded types.
pub trait Array2d where Self: Sized {
    type Item;
    fn shape(&self) -> (u32, u32);
    fn index(&self, position: (u32, u32)) -> Self::Item;
    
    fn map<U, F: Fn(Self::Item) -> U>(self, f: F) -> adapters::Map<Self, F, U> {
        adapters::Map::new(self, f)
    }

    /// Shapes must match.
    fn zip<U, O: Array2d<Item=U>>(self, other: O) -> adapters::Zip<Self, O> {
        adapters::Zip::new(self, other).unwrap()
    }
    
    /// Kernel f is applied to the neighborhood N of each point.
    /// See tests for usage.
    fn map_kernel<U, N, F>(self, f: F) -> adapters::Kernel<Self, F, N, U>
        where
            N: Neighborhood<Self::Item, Self>,
            F: Fn(&Self, N) -> U,
    {
        adapters::Kernel::new(self, f)
    }

    // Would be nice to have convolution here,
    // but neighborhood needs to have ::iter() for that.
    // Or directly ::convolve().
}

/// Describes some neighborhood around the position.
/// Ideally, this would contain a reference to the Array,
/// but then the Array lifetime seeps into map_kernel,
/// and prevents the neighborhood from being created.
/// I can't figure it out, so this ends up being just a cursor.
/// Also ideally, this would implement IntoIterator,
/// but it's so much easier to use impl Iterator that it's not even funny.
/// and trait methods are not compatible with impl Trait.
/// Meanwhile, I don't care about objects and dynamic dispatch here,
/// this is performance-sensitive code :S
pub trait Neighborhood<T, A: Array2d<Item=T>> {
    //type IntoIter: Iterator<Item=T>;
    fn new(position: (u32, u32)) -> Self;
    fn index(&self, array: &A) -> T;
    //fn into_iter(self) -> Self::IntoIter;
}

pub mod adapters {
    use super::*;
    
    use std::marker::PhantomData;
    
    /// Deferred processing
    pub struct Map<A, F, O> {
        array: A,
        f: F,
        out_type: PhantomData<O>,
    }
    
    impl<A, F, O> Map<A, F, O> {
        pub fn new(array: A, f: F) -> Self {
            Map {
                array,
                f,
                out_type: Default::default(),
            }
        }
    }

    impl<T, A: Array2d, F: Fn(A::Item) -> T> Array2d for Map<A, F, T> {
        type Item = T;
        fn shape(&self) -> (u32, u32) {
            self.array.shape()
        }
        
        fn index(&self, position: (u32, u32)) -> T {
            (self.f)(self.array.index(position))
        }
    }

    /// Paired pixels
    pub struct Zip<A, B> {
        one: A,
        other: B,
    }

    impl<A: Array2d, B: Array2d> Zip<A, B> {
        pub fn new(a: A, b: B) -> Option<Self> {
            if a.shape() == b.shape() {
                Some(Zip {one: a, other: b})
            } else {
                None
            }
        }
    }
    
    impl<A: Array2d, B: Array2d> Array2d for Zip<A, B> {
        type Item = (A::Item, B::Item);
        fn shape(&self) -> (u32, u32) {
            self.one.shape()
        }
        fn index(&self, position: (u32, u32)) -> Self::Item {
            (self.one.index(position), self.other.index(position))
        }
    }
    // Zip doesn't have .collect cause image crate is not meant for storing tuples.
    
    /// Process a kernel
    pub struct Kernel<A, F, N, T>
        where F: Fn(&A, N) -> T
    {
        array: A,
        f: F,
        neighborhood: PhantomData<N>,
    }

    impl<A: Array2d, F, N, T> Kernel<A, F, N, T>
        where F: Fn(&A, N) -> T
    {
        pub fn new(array: A, f: F) -> Self {
            Kernel {
                array,
                f,
                neighborhood: Default::default(),
            }
        }
    }
    
    impl<T, A, N, F, I> Array2d for Kernel<A, F, N, T>
        where
            A: Array2d<Item=I>,
            N: Neighborhood<I, A>,
            F: Fn(&A, N) -> T,
    {
        type Item = T;
        fn shape(&self) -> (u32, u32) {
            self.array.shape()
        }
        fn index(&self, position: (u32, u32)) -> T {
            (self.f)(&self.array, N::new(position))
        }
    }
    
    
    // Concrete definitions are needed only for collection.
    // FIXME: let consumers define them; this only uses Array2d::index.
    use image;
    use imageproc::map::map_colors;
    
    fn iter_array(shape: (u32, u32)) -> impl Iterator<Item=(u32, u32)> {
        let xs = 0..(shape.0);
        (0..(shape.1)).flat_map(move |y| xs.clone().map(move |x| (x, y)))
    }

    impl<A: Array2d, N, F> Kernel<A, F, N, u8>
        where
            Self: Array2d<Item=u8>,
            F: Fn(&A, N) -> u8,
    {
        pub fn collect(self) -> maps::BinaryMap {
            let shape = self.array.shape();
            let mut out = image::GrayImage::from_pixel(shape.0 as u32, shape.1 as u32, image::Luma::from([0]));
            for position in iter_array(shape) {
                *out.get_pixel_mut(position.0, position.1) = image::Luma::from([self.index(position)]);
            }
            out
        }
    }
    
    impl<A: Array2d, N, F> Kernel<A, F, N, u16>
        where
            Self: Array2d<Item=u16>,
            F: Fn(&A, N) -> u16,
    {
        pub fn collect(self) -> image::ImageBuffer<image::Luma<u16>, Vec<u16>> {
            let shape = self.array.shape();
            let mut out = image::ImageBuffer::from_pixel(shape.0 as u32, shape.1 as u32, image::Luma::from([0]));
            for position in iter_array(shape) {
                *out.get_pixel_mut(position.0, position.1) = image::Luma::from([self.index(position)]);
            }
            out
        }
    }

    impl<F, A: Array2d> Map<A, F, u8>
        where Self: Array2d<Item=u8>
    {
        pub fn collect(self) -> maps::BinaryMap {
            let shape = self.array.shape();
            let mut out = image::GrayImage::from_pixel(shape.0 as u32, shape.1 as u32, image::Luma::from([0]));
            for position in iter_array(shape) {
                *out.get_pixel_mut(position.0, position.1) = image::Luma::from([self.index(position)]);
            }
            out
        }
    }

    impl<F, A: Array2d> Map<A, F, u16>
        where Self: Array2d<Item=u16>
    {
        pub fn collect(self) -> image::ImageBuffer<image::Luma<u16>, Vec<u16>> {
            let shape = self.array.shape();
            let mut out = image::ImageBuffer::from_pixel(shape.0 as u32, shape.1 as u32, image::Luma::from([0]));
            for position in iter_array(shape) {
                *out.get_pixel_mut(position.0, position.1) = image::Luma::from([self.index(position)]);
            }
            out
        }
    }
}

/// This is quite awful, contains operation functions.
/// Use them like: Array2d::map(div(9))
///
/// Ideally, this would work sth like Array2d::div(9),
/// but there are several layers of problems with that.
/// First, ::div cannot expand to self.map(|foo| foo / 9),
/// because we don't evaluate eagerly.
/// That means we return the closure,
/// and need to have the closure type in the function signature.
/// This is not allowed, because the type is created inside the function, while the caller specifies the type.
/// https://github.com/rust-lang/rust/issues/51154
/// We also cannot do impl Fn(...) because that's not allowed in traits.
/// Finally, Box<dyn Fn(...)> blocks compiler optimizations, and we want this interface to be like Iterator.
/// We dont want to evaluate eagerly, because we change types, and we want to avoid allocations.
pub mod ops {
    use std::ops::Div;
    pub fn div<O: Copy, T: Div<O>>(value: O) -> impl Fn(T) -> T::Output {
        move |t| t / value
    }
}

/// A naive, sequentially processing functor.
pub struct Binary2d {
    pub data: maps::BinaryMap,
}

impl Binary2d {
    pub fn collect(self) -> maps::BinaryMap {
        self.data
    }
}

impl Array2d for Binary2d {
    type Item = u8;
    fn shape(&self) -> (u32, u32) {
        self.data.dimensions()
    }
    
    fn index(&self, position: (u32, u32)) -> Self::Item {
        use image::GenericImageView;
        unsafe { self.data.unsafe_get_pixel(position.0, position.1)[0] }
    }
}

impl IntoFunctor<Binary2d> for maps::BinaryMap {
    fn process(self) -> Binary2d {
        Binary2d {
            data: self,
        }
    }
}

struct Deferred2d(maps::BinaryMap);

#[cfg(test)]
mod test {
    use super::*;

    use imageproc::{ assert_pixels_eq, gray_image };
    use super::neighborhoods::Cross4;

    #[test]
    fn test_div() {
        let img = image::GrayImage::from_pixel(3, 3, image::Luma::from([16]));
        let out = img.process()
            .map(ops::div(8))
            .collect();
        assert_eq!(out, image::GrayImage::from_pixel(3, 3, image::Luma::from([2])));
    }

    #[test]
    fn test_div2() {
        let img = image::GrayImage::from_pixel(3, 3, image::Luma::from([16]));
        let out = img.process()
            .map(ops::div(2))
            .map(ops::div(2))
            .collect();
        assert_eq!(out, image::GrayImage::from_pixel(3, 3, image::Luma::from([4])));
    }
    
    #[test]
    fn test_type() {
        let img = image::GrayImage::from_pixel(3, 3, image::Luma::from([16]));
        let out = img.process()
            .map(u16::from)
            .collect();
        assert_eq!(out, image::ImageBuffer::from_pixel(3, 3, image::Luma::from([16])));
    }

    #[test]
    fn test_zip() {
        let a = image::GrayImage::from_pixel(1, 1, image::Luma::from([16]));
        let b = image::GrayImage::from_pixel(1, 1, image::Luma::from([3]));
        let out = a.process()
            .zip(b.process());
        assert_eq!(out.index((0, 0)), (16, 3));
    }

    #[test]
    fn test_zip_kernel() {
        let a = image::GrayImage::from_pixel(3, 3, image::Luma::from([16]));
        let b = a.clone();
        let out = a.process()
            .zip(b.process())
            .map_kernel(|array, cursor: Cross4| 1u8)
            .collect();
        //assert_eq!(out, image::ImageBuffer::from_pixel(3, 3, image::Luma::from([1])));
    }

    #[test]
    fn test_kernel() {
        let a = image::GrayImage::from_pixel(3, 3, image::Luma::from([16]));
        let out = a.process()
            .map_kernel(|array, cursor: Cross4| cursor.index(array))
            .collect();
        assert_eq!(out, image::ImageBuffer::from_pixel(3, 3, image::Luma::from([16])));
    }

    #[test]
    fn test_kernel_full() {
        let a = image::GrayImage::from_pixel(3, 3, image::Luma::from([1]));
        let out = a.process()
            .map_kernel(|array, cursor: Cross4| cursor.into_iter(array).sum::<u8>())
            .collect();
        assert_pixels_eq!(
            out,
            gray_image!(
                2, 3, 2;
                3, 4, 3;
                2, 3, 2
            )
        );
    }
}
