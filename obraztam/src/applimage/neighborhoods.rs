use super::*;

use std::iter::Sum;


fn iter_with_default<'a, T, A>(position: (u32, u32), shape: impl Iterator<Item=&'a (i64, i64)> + 'a, array: &'a A)
    -> impl Iterator<Item=T> + 'a
where 
    T: Default + 'a,
    A: Array2d<Item=T> + 'a
{
    shape.map(move |(x, y)| {
        let (posx, posy) = position;
        let x = x + posx as i64;
        let y = y + posy as i64;
        if x < 0 || x >= array.shape().0 as i64 {
            T::default()
        } else if y < 0 || y >= array.shape().1 as i64 {
            T::default()
        } else {
            array.index((x as u32, y as u32))
        }
    })
}

/// Cursor for the 4 neighboring cells.
/// Fills in out-of-bounds with default value.
pub struct Cross4 {
    position: (u32, u32),
}

impl Cross4 {
    pub fn into_iter<'a, T, A>(self, array: &'a A)
        -> impl Iterator<Item=T> + 'a
    where 
        T: Default + 'a,
        A: Array2d<Item=T> + 'a
    {
        let position = self.position;
        iter_with_default(position, [(-1, 0), (0, 1), (1, 0), (0, -1)].iter(), array)
    }
}

impl<T: Default, A: Array2d<Item=T>> Neighborhood<T, A> for Cross4 {
    fn new(position: (u32, u32)) -> Self {
        Self { position }
    }
    fn index(&self, array: &A) -> T {
        array.index(self.position)
    }
}

/// Cursor for the 8 neighboring cells.
/// Fills in out-of-bounds with default value.
pub struct Ring8 {
    position: (u32, u32),
}

impl Ring8 {
    pub fn into_iter<'a, T, A>(self, array: &'a A)
        -> impl Iterator<Item=T> + 'a
    where 
        T: Default + 'a,
        A: Array2d<Item=T> + 'a
    {
        let position = self.position;
        iter_with_default(
            position,
            [
                (-1, 0), (-1, 1), (0, 1), (1, 1), (1, 0), (1, -1), (0, -1), (-1, -1)
            ].iter(),
            array,
        )
    }

    pub fn convolve<T: Default + Sum, A: Array2d<Item=T>>(self, array: &A) -> T {
        self.into_iter(array).sum()
    }
}

impl<T: Default, A: Array2d<Item=T>> Neighborhood<T, A> for Ring8 {
    fn new(position: (u32, u32)) -> Self {
        Self { position }
    }
    fn index(&self, array: &A) -> T {
        array.index(self.position)
    }
}


