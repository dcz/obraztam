Obraztam
=========

A monadic 2d array (image) manipulation library.

It exposes the `Array2d` trait, which turns any 2d data structure into something resembling an iterator, with the familiar `.map(impl Fn(PixelIn) -> PixelOut)` method.

```
let processed = a
    .map(|i| i + 1)
    .collect::<Heap::<_>>();
```

The `collect` method serves the same role as in `Iterator`: it allows the compiler to optimize chained per-pixel calls before saving the result. This is fast:

```
let processed = a
    .map(|i| i + 1)
    .map(|i| i + 44)
    .collect::<Heap::<_>>();
```

In release mode, the above sample uses SIMD instructions. Fast!

While `.map()` method is the most useful, it's also least powerful. `.map_index()` is also exposed, accepting a `Fn(Index, PixelIn) -> PixelOut` to give you also access to the current index.

```
let processed = a
    .map_index(|i, p| {
        if i.0 < 200 {
            p
        } else {
            0
        }
    });
```

The most powerful of all, but also the most annoying to use, is the `.m()` method, giving access to the entire buffer on each call of the inner closure. It accepts `Fn(Index, &Array2d<Item=PixelIn>) -> PixelOut`.

Apart from giving access to all the data, `.m()` also allows to change the area of interest. For this, implement `obraztam::elementwise::Resize`. This makes it possible to keep correct bounds in operations like rescaling, or cropping, or erosion. For example, see `obraztam::elementwise::Shrink`.

```
let a = Stack::new_from_arr([3; 8], (4, 2)).unwrap();
        assert_eq!(
            a.m(Shrink(2))
                .collect::<Stack::<_, 8>>(),
            Stack::new_from_arr([3; 8], (2, 1)).unwrap()
        );
```

### Integration

Obraztam provides basic types in `obraztam::data`. It aims to be generic, so it does not integrate with anything, but the `świeci` demo program contains basic integration with the `image` crate.

Including in your project
-----------

Put this in your `Cargo.lock`:

```
[dependencies]
obraztam = { git = "https://framagit.org/dcz/obraztam.git" }
```

Running the laser beam extraction demo
---------- 

```
cd obraztam
cargo run --bin świeci -- sample.png lazors.png
```

Turns this image

![sample](sample.png)

into

![lazors](lazors.png)

TODO
-----

1. Use `par_iter` to split up pixel operations between cores. Should be really easy, because only 1 pixel is written at a time.
2. Compile into a fragment shader using [rust-gpu](https://github.com/EmbarkStudios/rust-gpu) - should be possible for the same reason.

Contact
-------


- [Blog & email](https://dcz_self.gitlab.io/)
- [Mastodon](https://fosstodon.org/@dcz)

License
---------

AGPL version 3.0 or later.
