use angular_units::Deg;
use image;
use obraztam::{Array2d, Index, MutArray};
use obraztam::data::Heap;
use prisma::{Hsv, Rgb};
use std::env::args;
use std::error::Error;

use image::GenericImageView;
use obraztam::Collectible;

struct ImageArray(image::DynamicImage);

impl Array2d for ImageArray {
    type Item = image::Rgba<u8>;
    fn shape(&self) -> Index {
        (self.0.width(), self.0.height())
    }
    fn index(&self, (x, y): Index) -> Self::Item {
        self.0.get_pixel(x, y)
    }
}

struct ImageDest(image::RgbImage);

impl MutArray<image::Rgb<u8>> for ImageDest {
    fn new((x, y): Index) -> Self {
        Self(image::RgbImage::new(x, y))
    }
    fn set(&mut self, (x, y): Index, v: image::Rgb<u8>) {
        self.0.put_pixel(x, y, v)
    }
}

fn main() {
    go(true).unwrap()
}
fn go(daylight: bool) -> Result<(), Box<dyn Error>> {
    let value_cond = if daylight {
        // cloudy indoors, filter out pink prints and wooden floor
        |v| v > 127.5
        // Harder to see the laser in daylight when other objects appear to have (the same) color.
        // It's worse to have false hits outside of the laser plane than miss some laser hits, so this filters out the uncertain hits,
        // even if it catches some laser hits.
    } else {
        |_| true
    };
    
    let filename = args().skip(1).next().unwrap();
    let img = image::io::Reader::open(filename)?.decode()?;
    let img = ImageArray(img)
        .map(|p| Rgb::new(p.0[0] as f32, p.0[1] as f32, p.0[2] as f32))
        .map(|p| Hsv::<_, Deg<f32>>::from(p))
        .map_index(|_i, p| {
            if (p.hue() < Deg(30.0) || p.hue() > Deg(300.0))
                && p.saturation() > 0.5 && value_cond(p.value())
            {
                p.value() as u8
            } else {
                0
            }
        })
        .map(|p| image::Rgb::from([p, p, p]))
        .collect::<ImageDest>();
    let filename = args().skip(2).next().unwrap();
    img.0.save(filename)?;
    Ok(())
}
